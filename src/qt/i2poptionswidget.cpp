#include "i2poptionswidget.h"
#include "ui_i2poptionswidget.h"

#include "optionsmodel.h"
#include "monitoreddatamapper.h"
#include "showi2paddresses.h"
//#include "i2p.h"
#include "util.h"
#include "clientmodel.h"


I2POptionsWidget::I2POptionsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::I2POptionsWidget),
    clientModel(0)
{
    ui->setupUi(this);

    QObject::connect(ui->pushButtonCurrentI2PAddress,  SIGNAL(clicked()), this, SLOT(ShowCurrentI2PAddress()));
    QObject::connect(ui->pushButtonGenerateI2PAddress, SIGNAL(clicked()), this, SLOT(GenerateNewI2PAddress()));

    QObject::connect(ui->checkBoxUseI2POnly           , SIGNAL(stateChanged(int))   , this, SIGNAL(settingsChanged()));
    QObject::connect(ui->lineEditOurHost              , SIGNAL(textChanged(QString)), this, SIGNAL(settingsChanged()));
    QObject::connect(ui->lineEditTunnelName           , SIGNAL(textChanged(QString)), this, SIGNAL(settingsChanged()));
    QObject::connect(ui->spinBoxInboundLength         , SIGNAL(valueChanged(int))   , this, SIGNAL(settingsChanged()));
    QObject::connect(ui->spinBoxInboundQuantity       , SIGNAL(valueChanged(int))   , this, SIGNAL(settingsChanged()));
    QObject::connect(ui->spinBoxOutboundLength        , SIGNAL(valueChanged(int))   , this, SIGNAL(settingsChanged()));
    QObject::connect(ui->spinBoxOutboundQuantity      , SIGNAL(valueChanged(int))   , this, SIGNAL(settingsChanged()));
    QObject::connect(ui->spinBoxOurPort               , SIGNAL(valueChanged(int))   , this, SIGNAL(settingsChanged()));
}

I2POptionsWidget::~I2POptionsWidget()
{
    delete ui;
}

void I2POptionsWidget::setMapper(MonitoredDataMapper& mapper)
{
    mapper.addMapping(ui->checkBoxUseI2POnly           , OptionsModel::I2PUseI2POnly);
    mapper.addMapping(ui->lineEditOurHost              , OptionsModel::I2POurHost);
    mapper.addMapping(ui->spinBoxOurPort               , OptionsModel::I2POurPort);
    mapper.addMapping(ui->lineEditTunnelName           , OptionsModel::I2PSessionName);
    mapper.addMapping(ui->spinBoxInboundQuantity       , OptionsModel::I2PInboundQuantity);
    mapper.addMapping(ui->spinBoxInboundLength         , OptionsModel::I2PInboundLength);
    mapper.addMapping(ui->spinBoxOutboundQuantity      , OptionsModel::I2POutboundQuantity);
    mapper.addMapping(ui->spinBoxOutboundLength        , OptionsModel::I2POutboundLength);
}

void I2POptionsWidget::setModel(ClientModel* model)
{
    clientModel = model;
}

void I2POptionsWidget::ShowCurrentI2PAddress()
{
    if (clientModel)
    {
        const QString pub = clientModel->getPublicI2PKey();
        const QString priv = clientModel->getPrivateI2PKey();
        const QString b32 = clientModel->getB32Address(pub);
        const QString configFile = QString::fromStdString(GetConfigFile().string());

        ShowI2PAddresses i2pCurrDialog("Your current I2P-address", pub, priv, b32, configFile, this);
        i2pCurrDialog.exec();
    }
}

void I2POptionsWidget::GenerateNewI2PAddress()
{
    if (clientModel)
    {
        QString pub, priv;
        clientModel->generateI2PDestination(pub, priv);
        const QString b32 = clientModel->getB32Address(pub);
        const QString configFile = QString::fromStdString(GetConfigFile().string());

        ShowI2PAddresses i2pCurrDialog("Generated I2P address", pub, priv, b32, configFile, this);
        i2pCurrDialog.exec();
    }
}


